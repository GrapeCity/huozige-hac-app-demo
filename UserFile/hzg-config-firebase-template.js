// Init Firebase for APM
 (async () => {

    // Load Firebase modules
    const { initializeApp } = await import('https://www.gstatic.com/firebasejs/11.0.2/firebase-app.js');
    const { getAnalytics } = await import('https://www.gstatic.com/firebasejs/11.0.2/firebase-analytics.js');
    const { getPerformance } = await import("https://www.gstatic.com/firebasejs/11.0.2/firebase-performance.js");

    const firebaseConfig = {
      apiKey: "AIzaSyCjpFl39NCZf4RJe4Ug0wNzfIS7ARIz6NU",
      authDomain: "hac-demo.firebaseapp.com",
      projectId: "hac-demo",
      storageBucket: "hac-demo.firebasestorage.app",
      messagingSenderId: "254752917489",
      appId: "1:254752917489:web:4cdbae1fd3e04e99c56650",
    };

    // Initialize Firebase
    const app = initializeApp(firebaseConfig);
    const analytics = getAnalytics(app);
    const perf = getPerformance(app);
  })();