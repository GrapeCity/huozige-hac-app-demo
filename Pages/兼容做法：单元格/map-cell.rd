{
  "PageType": 0,
  "ColumnCount": 15,
  "RowCount": 25,
  "CustomNames": [
    {
      "Name": "container",
      "Formula": "'map-cell'!$B$11"
    },
    {
      "Name": "lat",
      "Formula": "'map-cell'!$E$7"
    },
    {
      "Name": "lon",
      "Formula": "'map-cell'!$E$9"
    },
    {
      "Name": "err",
      "Formula": "'map-cell'!$B$10"
    }
  ]
}