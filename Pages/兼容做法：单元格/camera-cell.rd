{
  "PageType": 0,
  "ColumnCount": 15,
  "RowCount": 24,
  "Formulas": {
    "10,1": "\"测试水印 \"&NOW()"
  },
  "CustomNames": [
    {
      "Name": "ImageCell",
      "Formula": "'camera-cell'!$B$23"
    },
    {
      "Name": "LabelWidthCell",
      "Formula": "'camera-cell'!$G$6"
    },
    {
      "Name": "ResultUriCell",
      "Formula": "'camera-cell'!$B$9"
    },
    {
      "Name": "TAGID",
      "Formula": "'camera-cell'!$B$7"
    }
  ]
}