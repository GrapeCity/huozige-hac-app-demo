{
  "PageType": 0,
  "ColumnCount": 16,
  "RowCount": 17,
  "CustomNames": [
    {
      "Name": "LabelWidthCell",
      "Formula": "'esc-cell'!$G$8"
    },
    {
      "Name": "LabelHeightCell",
      "Formula": "'esc-cell'!$G$10"
    },
    {
      "Name": "printersCell",
      "Formula": "'esc-cell'!$P$6"
    },
    {
      "Name": "DPICell",
      "Formula": "'esc-cell'!$P$8"
    }
  ]
}