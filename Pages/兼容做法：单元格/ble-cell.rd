{
  "PageType": 0,
  "ColumnCount": 16,
  "RowCount": 29,
  "CustomNames": [
    {
      "Name": "LabelWidthCell",
      "Formula": "'ble-cell'!$G$6"
    },
    {
      "Name": "TAGID",
      "Formula": "'ble-cell'!$B$7"
    },
    {
      "Name": "ScanResultCell",
      "Formula": "'ble-cell'!$P$4"
    },
    {
      "Name": "ReadResultCell",
      "Formula": "'ble-cell'!$P$6"
    },
    {
      "Name": "WriteResultCell",
      "Formula": "'ble-cell'!$P$13"
    },
    {
      "Name": "ErrorCell",
      "Formula": "'ble-cell'!$B$28"
    },
    {
      "Name": "ReadRawResultCell",
      "Formula": "'ble-cell'!$P$9"
    },
    {
      "Name": "WatchCell",
      "Formula": "'ble-cell'!$B$22"
    }
  ]
}