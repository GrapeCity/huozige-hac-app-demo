{
  "PageType": 0,
  "ColumnCount": 16,
  "RowCount": 17,
  "CustomNames": [
    {
      "Name": "LabelWidthCell",
      "Formula": "esc!$G$8"
    },
    {
      "Name": "LabelHeightCell",
      "Formula": "esc!$G$10"
    },
    {
      "Name": "DPICell",
      "Formula": "esc!$P$8"
    }
  ]
}